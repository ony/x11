# Copyright 2008 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2013 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

MY_PNV="LibVNCServer-${PV}"

require github [ user=LibVNC tag=${MY_PNV} ] \
        autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ] \
        option-renames [ renames=[ 'gnutls providers:gnutls' ] ]

SUMMARY="Library for easy implementation of a RDP/VNC (Remote Desktop Protocol/Virtual Network Computing) server"
DESCRIPTION="
VNC is a set of programs using the RFB (Remote Frame Buffer) protocol. They are
designed to "export" a frame buffer via net. It is already in wide use for
administration, but it is not that easy to program a server yourself.
"

BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS+=" freecode:${PN}"

LICENCES="GPL-2"
SLOT="0"
MYOPTIONS="
    ( providers: gnutls libressl openssl ) [[ number-selected = exactly-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/libpng:=
        sys-libs/zlib
        providers:eudev? ( sys-apps/eudev )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        providers:gnutls? (
            dev-libs/gnutls[>=2.4.0]
            dev-libs/libgcrypt[>=1.4.0]
        )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        providers:systemd? ( sys-apps/systemd )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-static
    --with-crypt
    --with-ipv6
    --with-jpeg
    --with-png
    --with-pthread
    --with-websockets
    --with-zlib
    --without-sdl-config
)
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    providers:gnutls
    "providers:gnutls gcrypt"
    "providers:gnutls client-gcrypt"
    "!providers:gnutls crypto"
    "!providers:gnutls ssl"
)

