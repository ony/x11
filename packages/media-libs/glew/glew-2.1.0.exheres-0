# Copyright 2009, 2010, 2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ suffix=tgz ]

SUMMARY="The OpenGL Extension Wrangler Library (GLEW) is a cross-platform open-source C/C++ extension loading library"
DESCRIPTION="
GLEW contains support up to OpenGL 4.6 and the following extensions:
* OpenGL extensions
* WGL extensions
* GLX extensions
"

UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/log.html"

LICENCES="
    BSD-3
    GPL-2 [[ note = [ Automatic code generation scripts ] ]]
    MIT
"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        x11-dri/mesa[>=9]
        x11-libs/libX11
    built-against:
        x11-dri/glu[>=9] [[ note = [ Requires: glu in glew.pc ] ]]
"

DEFAULT_SRC_COMPILE_PARAMS=(
    CC=${CC}
    LD=${CC}
    STRIP=:
    OPT="${CFLAGS}"
    LDFLAGS.EXTRA="${LDFLAGS}"
)

# GLEW_DEST is used as prefix for installing, GLEW_PREFIX is used as ${prefix} in glew.pc.in
DEFAULT_SRC_INSTALL_PARAMS=(
    DESTDIR="${IMAGE}"
    GLEW_DEST=/usr/$(exhost --target)
    GLEW_PREFIX=/usr/$(exhost --target)
    LIBDIR=/usr/$(exhost --target)/lib
    STRIP=:
)

src_prepare() {
    edo sed \
        -e '/INSTALL.*LIB.STATIC/d' \
        -e 's/\$(INSTALL) -s/$(INSTALL)/' \
        -i Makefile

    default
}

src_install() {
    emake ${DEFAULT_SRC_INSTALL_PARAMS[@]} install.all
    dodoc doc/*.{css,html,jpg,png}
}

