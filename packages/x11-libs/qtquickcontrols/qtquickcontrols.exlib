# Copyright 2014-2016 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework: A set of controls for Qt Quick"
DESCRIPTION="The Qt Quick Controls module provides a set of controls that can
be used to build complete interfaces in Qt Quick."

LICENCES+=" GPL-2"
MYOPTIONS="doc examples"

DEPENDENCIES="
    build:
        doc? ( x11-libs/qttools:${SLOT} )
    build+run:
        x11-libs/qtbase:${SLOT}[>=${PV}][gui]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
        x11-libs/qtgraphicaleffects:${SLOT}
        !x11-libs/qtdeclarative:5[<5.7.0-rc] [[
            description = [ Parts of qtquickcontrols have been move to qtdeclarative ]
            resolution = uninstall-blocked-after
        ]]
"

qtquickcontrols_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qtquickcontrols_src_compile() {
    default

    option doc && emake docs
}

qtquickcontrols_src_install() {
    default

    if option doc ; then
        dodoc doc/qtquickcontrols.qch
        docinto html
        dodoc -r doc/qtquickcontrols
    fi
}

