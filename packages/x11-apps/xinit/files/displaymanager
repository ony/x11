#!/sbin/runscript
# Copyright 2008 Bo Ørsted Andresen
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'xdm.initd-1' from Gentoo, which is:
#     Copyright 1999-2004 Gentoo Foundation

depend() {
   need localmount

   after bootmisc gpm netmount consolefont dbus
   before alsasound net.lo

   use acpid
}

start() {
   case "${DISPLAYMANAGER}" in
      gdm|gnome) BIN=/usr/sbin/gdm-binary           ;;
      kdm|kde)   BIN=/usr/bin/kdm                   ;;
      slim)      BIN=/usr/bin/slim                  ;;
      xdm)       BIN=/usr/bin/xdm                   ;;
      *)         BIN=$(type -P "${DISPLAYMANAGER}") ;;
   esac

   if [[ ! -x ${BIN} ]]; then
      local x

      BIN=
      for x in "${DISPLAYMANAGER}" xdm gdm kdm slim ; do
         [[ -x ${BIN:=$(type -P "${x}")} ]] && break
         BIN=
      done

      if [[ -z ${BIN} ]]; then
         eerror "Could not locate an executable Display Manager"
         return 1
      fi
   fi

   save_options bin "${BIN}"

   ebegin "Starting ${BIN##*/}"
   start-stop-daemon --start --exec "${BIN}" --pidfile "/run/displaymanager.pid" \
                     --name "${BIN##*/}"
   eend $?
}

stop() {
   case "${DISPLAYMANAGER}" in
      gdm)
         /usr/sbin/gdm-stop
      ;;
      *)
         local binargs=() BIN=$(get_options bin)

         ebegin "Stopping ${BIN##*/:-displaymanager}"

         if [[ -n ${BIN} ]] ; then
            binargs=( --exec "${BIN}" --name "${BIN##*/}" )
         fi

         start-stop-daemon --stop "${binargs[@]}" --pidfile "/run/displaymanager.pid"
         eend $?
      ;;
   esac
}

restart()
{
   case "${DISPLAYMANAGER}" in
      gdm)
         /usr/sbin/gdm-safe-restart
      ;;
   esac
}

